from dataclasses import dataclass


@dataclass
class RepositoryData:
    _id: str
    full_repository_name: str
    repository_link: str
    repository_creation_date: str
    repository_license: dict
    repository_last_pull: str
    repository_last_issue: str
    repository_language_percentage: dict
