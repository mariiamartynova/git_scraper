import requests
from requests.exceptions import ProxyError
import mongodb_storage
import const
from dataclasses import asdict
from repository_dataclass import RepositoryData

SEARCH_API_LINK = f"{const.GIT_API}/search/repositories?q=stars:{const.STARS_COUNT}"


def get_json(link: str) -> dict:
    for attempt in range(const.ATTEMPT):
        try:
            proxy_dict = {"http": const.PROXIES, "https": const.PROXIES}
            response = requests.request(
                "GET",
                link,
                headers=const.HEADERS,
                proxies=proxy_dict if const.USER_PROXY else None,
            )
            content = response.json()
            return content
        except:
            continue
    raise ProxyError


def get_repository_data(json_data: dict) -> list:
    repositories_dict = json_data["items"]
    repositories_list = list()
    for repository_dict in repositories_dict:
        repository_dataclass = RepositoryData(
            repository_dict["name"],
            repository_dict["full_name"],
            repository_dict["html_url"],
            repository_dict["created_at"],
            repository_dict["license"],
            get_last_pull(repository_dict["full_name"]),
            get_last_issue(repository_dict["full_name"]),
            get_language_percentage(repository_dict["full_name"]),
        )
        repositories_list.append(asdict(repository_dataclass))
    return repositories_list


def get_last_issue(repository_name: str) -> str:
    return get_last_data(repository_name, "issues")


def get_last_pull(repository_name: str) -> str:
    return get_last_data(repository_name, "pulls")


def get_language_percentage(repository_name: str) -> dict:
    language_api_link = f"{const.GIT_API}/repos/{repository_name}/languages"
    language_api_dict = get_json(language_api_link)
    language_dict = {}
    if len(language_api_dict) != 0:
        keys = [*language_api_dict]
        values = list(language_api_dict.values())
        values_sum = sum(values)
        values = list(map(lambda x: (x * 100) / values_sum, values))
        language_dict = dict(zip(keys, values))
    return language_dict


def get_last_data(repository_name: str, suffix: str) -> str:
    suffix_api_link = f"{const.GIT_API}/repos/{repository_name}/{suffix}?per_page=1"
    last_data = get_json(suffix_api_link)[0]["created_at"]
    return last_data


def run():
    json_data = get_json(SEARCH_API_LINK)
    repositories_data = get_repository_data(json_data)
    print(repositories_data)
    mongo_instance = mongodb_storage.MongoDBStorage()
    for repo in repositories_data:
        mongo_instance.run(repo)


if __name__ == "__main__":
    run()
